from django.db import models
from datetime import datetime

# Create your models here.
class Post(models.Model):
    post_time = models.DateTimeField(default=datetime.now)
    post_text = models.TextField()

    #OrderBy post_time for displaying on page
    def __str__(self):
        return "{}\nPost Time: {}".format(self.post_text,self.post_time)

class Comment(models.Model):
    post_time = models.DateTimeField(default=datetime.now)
    post_text = models.TextField()
    linked_post = models.ForeignKey(Post,on_delete=models.CASCADE)
    votes = models.IntegerField(default=0)
    #OrderBy votes for displaying on page
    def __str__(self):
        return "Votes: {} - {}\nTime Posted: {}".format(self.votes,self.post_text,self.post_time)