from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .models import Post, Comment
from datetime import datetime

def index(request):
    if(request.POST):
        print(request.POST)
        link_post = Post.objects.get(id=request.POST["post_instance"])
        print()
        print(link_post)
        print()
        c = Comment()
        c.linked_post = link_post
        c.post_time = datetime.now()
        c.post_text = request.POST["new_comment"]
        c.votes = 0
        c.save(force_insert=True)
    posts = Post.objects.all()[:10]
    data = []
    for p in posts:
        print(p.id)
        post_comments = Comment.objects.filter(linked_post_id=p.id)
        data.append({"post":p,"comments":post_comments})
        print(data)
    
    # for p in posts:
    #     for comment in comments
    #         print('echo')

    context = {
        'posts':data
    }
    #latest = Post.objects.order_by('post_time')[:10]
    return render(request,'blog1/posts.html',context)

